import mongoose from "mongoose";
import Product from "../models/product.js";


/**
 * getproducts function fetch all the products in the database
 * fetch the products
 * shows error if any
 * @param {*} req  is the request from the client
 * @param {*} res  id the response sent from the server
 */
export const getProducts = async (req, res) => {
  try {
    const postProduct = await Product.find();
    console.log(postProduct);
    res.status(200).json(postProduct);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

/**
 * createProduct function create product and save in the database
 * saves the data into the database 
 * shows error if any 
 * @param {*} req  is the request from the client
 * @param {*} res  id the response sent from the server
 */
export const createProduct = async (req, res) => {
  const post = req.body;

  const newPost = new Product(post);
  try {
    await newPost.save();
    res.status(201).json(newPost);
  } catch (error) {
    res.status(409).json({ message: error.message });
  }
};

/**
 * updateProduct function updates the product 
 * checks if the id sent from the client is valid 
 * update the data in the database
 * shows error if any 
 * @param {*} req  is the request from the client
 * @param {*} res  id the response sent from the server
 */
export const updateProduct = async (req, res) => {
  const { id: _id } = req.params;

  const product = req.body;

  //checking if the id is mongoose id
  if (!mongoose.Types.ObjectId.isValid(_id))
    return res.status(404).send("No product with that id");

  const updatedPost = await Product.findByIdAndUpdate(
    _id,
    { ...product, _id },
    { new: true }
  );
  res.json(updatedPost);
};

/**
 * deleteProduct function delete the post in the database
 * checks if the id sent from the client is valid 
 * @param {*} req is the request from the client
 * @param {*} res is the response sent from the server
 */
export const deleteProduct = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).send("No product with that id");

  await Product.findByIdAndDelete(id);

  res.send("deleted successfully");
};


export const getProductById = async (req,res)=>{
  const {id} = req.params 
  if (!mongoose.Types.ObjectId.isValid(id))
  return res.status(404).send("No product with that id");
  
  try {
    const product = await Product.findById(id);
    console.log(product);
    res.status(200).json(product);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }

}
