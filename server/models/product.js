import mongoose from "mongoose"; // importing mongoose

/**
 * creating schema for the product
 */
const productSchema = mongoose.Schema({
  name: String,
  description: String,
  price: Number,
  seller: String,
  quantity: Number,
  
});

//creating model for product schema
const Product = mongoose.model("Product", productSchema);

//exporting product model
export default Product;
