import React from "react";
import {Route,Switch,BrowserRouter} from 'react-router-dom'


import Home from "./components/Home/home";
import Form from "./components/Form/Form";
import Products from "./components/Products/Products";
import UpdateProduct from "./components/UpdateProduct/updateProduct";
import Success from "./components/success/success";

import "./App.css";
import NavBar from "./components/NavBar/nav";

function App() {
  

  return (
     <BrowserRouter>
     <NavBar/>
       <Switch>
         <Route exact path ='/' component={Home} />
          <Route exact path ='/products' component={Products}/>
          <Route exact path ='/addProducts' component={Form} />
          <Route exact path="/updateProduct/:id" component={UpdateProduct}/>
          <Route exact path="/success" component={Success}/>
       </Switch>
     </BrowserRouter>

  );
}

export default App;
