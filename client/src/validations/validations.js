export function validate_name(name) {
  let error = [];

  if (!name) {
    error.push("name is mandatory.");
    return error;
  }
  if (! name.match(/^[a-zA-Z ]{5,30}$/)) {
    error.push("product name length should be more than 5 and less than 30");
    return error;
  }

  return error;
}

export function validate_desription(description) {
  let error = [];

  if (!description) {
    error.push("description is mandatory.");
    return error;
  }
  if (!description.match(/^[a-zA-Z ]{10,30}$/)) {
    error.push("product description length should be more than 10 and less than 30");
    return error;
  }

  return error;
}


export function validate_quantity(quantity){
  let error = [];

  if (!quantity) {
    error.push("quantity is mandatory.");
    return error;
  }
  if (quantity <= 0 ) {
    error.push("quantity cannot not be negative or equal to 0");
    return error;
  }

  return error;
}

export function validate_seller(seller) {
  let error = [];

  if (!seller) {
    error.push("seller is mandatory.");
    return error;
  }
  if (typeof seller != "string") {
    // check if param is string
    error.push(
      "seller is not a string " + seller
    );
    return error;
  }
  // if (!seller.match(/^[a-zA-Z]{10,30}$/)) {
  //   error.push("product description length should be more than 10 and less than 30");
  //   return error;
  // }

  return error;
}

export function validate_price(price){
  let error = [];

  if (!price) {
    error.push("price is mandatory.");
    return error;
  }
  if (price <= 0 ) {
    error.push("price cannot not be negative or equal to 0");
    return error;
  }
  return error;
}

