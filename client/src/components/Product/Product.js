import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";

import { deleteProducts } from "../../redux/actions/products";
import { AiFillDelete, AiOutlineEdit } from "react-icons/ai";

import "./product.css";

const Product = ({ product }) => {
  // const [currentId,setCurrentId] = useState()
  const dispatch = useDispatch();

  const confirmDelete = ()=>{
    
    alert("are you sure you want to delete the product")
    deletepost()
  }

  const deletepost = () => {
   
    dispatch(deleteProducts(product._id));
  };

  return (
    <div className="product-card">
      <div className="product-details">
        <p className="product-catagory">Name: {product.name}</p>
        <p>Description: {product.description}</p>
        <div>
          <p>Seller: {product.seller}</p>
          <p>Quantity: {product.quantity}</p>
        </div>
        <div>
          <h4>Price: {product.price}</h4>
        </div>
      </div>
      <div className="product-links">
        <a className="update-icon">
          <Link
            to={
              `/updateProduct/${product._id}`
              // pathname: "/updateProduct",
              // query:{id:product._id}
            }
          >
            <AiOutlineEdit />
          </Link>
        </a>
        <a href="" className="delete-icon"  onClick={confirmDelete}>
          <AiFillDelete />
        </a>
      </div>
    </div>
  );
};

export default Product;
