import {useEffect,useState } from "react";
import { useParams ,useHistory} from "react-router-dom";
import { getProductById ,updateProducts} from "../../api";


import "./updateProduct.css";

const UpdateProduct = ()=>{
  const [product,setProduct] = useState({
    name:'',description:'',price:'',seller:'',quantity:''
  })
  // const {location} = this.props
  // const {query} = location
  // const {id} = query
  const {id} = useParams()
  let history = useHistory();

  // console.log(id)
  
 useEffect(()=>{
   getProduct();
 },[] )

  const getProduct = async()=>{
    const response = await getProductById(id)
    setProduct(response.data)
  }

  const updateProduct = async() =>{
    const response =await updateProducts(id, product)
    console.log(response)
    history.push('/products')
    // setProduct({ name:'',description:'',price:'',seller:'',quantity:''})
  }

return(
  <div className="form-container">
  <h1>Update Product</h1>
  <form>
    <label for="name"> Name:</label>
    <input type="text" id="name" onChange={(event)=> setProduct({...product,name:event.target.value})} value={product.name} />
    <label for="description">Description:</label>
    <input type="text" id="description" onChange={(event)=> setProduct({...product,description:event.target.value})} value={product.description} />
    <label for="price">Price:</label>
    <input type="number" id="price" onChange={(event)=> setProduct({...product,price:event.target.value})} value={product.price} />
    <label for="seller">Seller:</label>
    <input type="text" id="seller" onChange={(event)=> setProduct({...product,seller:event.target.value})} value={product.seller} />
    <label id="quantity">Quantity:</label>
    <input type="number" id="quantity" onChange={(event)=> setProduct({...product,quantity:event.target.value})} value={product.quantity} />
    <button className="updateform-button" type="button" onClick={()=> updateProduct()}>Update Product</button>
  </form>
</div>

)

}

export default UpdateProduct;
