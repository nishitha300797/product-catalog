import React from "react";
import { Link } from "react-router-dom";

import "./nav.css";
const NavBar = () => {
  return (
    <div className="header-container">
      <div className="test">
        <input type="checkbox" className="menu-toggle" id="menu-toggle" />
        <div className="mobile-bar">
          <label htmlFor="menu-toggle" className="menu-icon">
            <span />
          </label>
        </div>
        <div className="header">
          <nav>
            <ul>
              <li>
                <Link to="/">
                  <a href="#">Home</a>
                </Link>
              </li>
              <li>
                <Link to="/products">
                  <a href="#">Products</a>
                </Link>
              </li>

              <li>
                <Link to="/addProducts">
                  <a>Add Products</a>
                </Link>
              </li>
              {/* <li>
                <Link to="/updateProduct">
                  <a>Update Product</a>
                </Link>
              </li> */}
            </ul>
          </nav>
        </div>
        <a className="navbar-brand" style={{ padding: 0, width: "40%" }}>
          <img src="logo.jpg" className="icon-style" />
        </a>
      </div>
    </div>
  );
};

export default NavBar;
