import { useSelector, useDispatch } from "react-redux";
import React, { useEffect} from "react";

import Product from "../Product/Product";
import { getProducts } from "../../redux/actions/products";

import "./products.css";

const Products = () => {

  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  const products = useSelector((state) => state.products);
  console.log("product=>",products[0]);


  

  // console.log(products);
  // console.log("error",error)
  return (
    <>
      <div className="product-container">
        {products.map((product) => (
          <Product key={product._id} product={product} />
        ))}
      </div>
   
    </>
  );
};

export default Products;
