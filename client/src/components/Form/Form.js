import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import "./form.css";

import { createProduct } from "../../redux/actions/products";
import {
  validate_name,
  validate_desription,
  validate_price,
  validate_quantity,
  validate_seller,
} from "../../validations/validations";

const Form = () => {
  const [productData, setProduct] = useState({
    name: "",
    description: "",
    price: "",
    seller: "",
    quantity: "",
  });
  const [error, setError] = useState({
    name_error: "",
    description_error: "",
    price_error: "",
    seller_error: "",
    quantity_error: "",
  });

  const dispatch = useDispatch();
  let history = useHistory();
  const handleSubmit = (event) => {
    event.preventDefault();
    const nameError = validate_name(productData.name);
    const descriptionError = validate_desription(productData.description);
    const quantityError = validate_quantity(productData.quantity);
    const sellerError = validate_seller(productData.seller);
    const priceError = validate_price(productData.price);

    if (
      nameError.length !== 0 ||
      descriptionError.length !== 0 ||
      quantityError.length !== 0 ||
      sellerError.length !== 0 ||
      priceError.length !== 0
    ) {
      setError({
        name_error: nameError,
        description_error: descriptionError,
        quantity_error: quantityError,
        seller_error: sellerError,
        price_error: priceError,
      });
      console.log(nameError.length)
    } else {
      dispatch(createProduct(productData));

      history.push("/success");

      console.log(productData);

      setProduct({
        name: "",
        description: "",
        price: "",
        seller: "",
        quantity: "",
      });
    }
  };

  return (
    <>
      <div className="form-container">
        <h1>Add Product</h1>
        <form onSubmit={handleSubmit}>
          <label for="name"> Name:</label>
          <input
            type="text"
            id="name"
            value={productData.name}
            onChange={(e) =>
              setProduct({ ...productData, name: e.target.value })
            }
          />
          <p className="form-error">{error.name_error}</p>
          <label for="description">Description:</label>
          <input
            type="text"
            id="description"
            value={productData.description}
            onChange={(e) =>
              setProduct({ ...productData, description: e.target.value })
            }
          />
          <p className="form-error">{error.description_error}</p>
          <label for="price">Price:</label>
          <input
            type="number"
            id="price"
            value={productData.price}
            onChange={(e) =>
              setProduct({ ...productData, price: e.target.value })
            }
          />
          <p className="form-error">{error.price_error}</p>

          <label for="seller">Seller:</label>
          <input
            type="text"
            id="seller"
            value={productData.seller}
            onChange={(e) =>
              setProduct({ ...productData, seller: e.target.value })
            }
          />
          <p className="form-error">{error.seller_error}</p>

          <label id="quantity">Quantity:</label>
          <input
            type="number"
            id="quantity"
            value={productData.quantity}
            onChange={(e) =>
              setProduct({ ...productData, quantity: e.target.value })
            }
          />
          <p className="form-error">{error.quantity_error}</p>

          {/* <FileBase64
        multiple={ false }
        onDone={ ({base64})=>setProduct({...productData,image:base64})} /> */}
          <button className="form-button" type="submit">
            Add Product
          </button>
        </form>
      </div>
    </>
  );
};

export default Form;
