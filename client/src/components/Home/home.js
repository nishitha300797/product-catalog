import React from "react";

import "./home.css";

const Home = () => {
  return (
    <>
      <div className="home-container">
        <div>
          <img src="home_wallpaper.webp" alt="home" />
        </div>
      </div>
    </>
  );
};

export default Home;
