import{FETCH_ALL , CREATE , DELETE , UPDATE,ERROR} from '../constants/actionTypes'

// const productState = {
//   products:[],
//   error:''
// }
export default (products=[],action)=>{
  switch(action.type){
    case UPDATE:
      return products.map((product)=> product._id === action.payload._id ? action.payload : product)
    case FETCH_ALL:
      return action.payload
    case CREATE:
      return [...products,action.payload]
    case DELETE :
      return products.filter((product)=>product._id !== action.payload)
    case ERROR:
      return [...products,action.payload]

    default:
      return products

  }
}