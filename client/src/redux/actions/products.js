import * as api from "../../api/index";
import { FETCH_ALL,CREATE ,UPDATE,DELETE,ERROR} from "../constants/actionTypes";

//action creator for fetching products
 const getProducts = () => async (dispatch) => {
  try {

    //fetch all the data from api
    //response from the api
    const { data } = await api.fetchProducts();

    //action for fetching the products
    const action = { type: FETCH_ALL , payload: data };
    
    dispatch(action);
  } catch (error) {
       const action = {type:ERROR,payload:error}
    // console.log(error.message);
    dispatch(action)
  }
};

// action creator for creating product
const createProduct = (post) => async (dispatch) => {
  try {
    const { data } = await api.createProduct(post);
    
    //action for creating a product
    const action = { type:CREATE, payload: data };
    
    dispatch(action);
  } catch (error) {
    console.log(error.message);
  }
};

// action creator for updating a product
const updateProducts = (id, post) => async (dispatch) => {
  try {
    const { data } = await api.updateProducts(id, post);

    //action for updating a product
    const action = { type: UPDATE, payload: data };

    dispatch(action);
  } catch (error) {
    console.log(error.message);
  }
};


// action creator for deleting a product
const deleteProducts = (id) => async (dispatch) => {
  try {
    await api.deleteProducts(id);
    
    //action for deleting the products
    const action = { type: DELETE, payload: id };

    dispatch(action);
  } catch (error) {
    console.log(error.message);
  }
};

//exporting action creators
export { createProduct, getProducts, deleteProducts, updateProducts };
