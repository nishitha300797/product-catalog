import axios from 'axios'


const url ="http://localhost:5000/products"

export const fetchProducts =()=> axios.get(url)

export const createProduct =(newProduct) => axios.post(url,newProduct)

export const updateProducts =(id,updatedProduct) => axios.patch(`${url}/${id}`,updatedProduct)

export const deleteProducts = (id) => axios.delete(`${url}/${id}`)

export const getProductById = (id) =>axios.get(`${url}/${id}`)

